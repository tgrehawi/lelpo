require "util"

token = {}

function token:tostring()
	return string.format(self.color.."%s#", tostring(self.value))
end

local parser = {}

function parser:tostring()
	return string.format("#fparser#["..self.color.."%s#%s]", self.name,
	self.staticvalue and string.format("#f(#$#b%s#$#f)#", self.staticvalue) or "")
end

local parsermt = {
	__index = parser,
	__tostring = parser.tostring
}

local function newparser(name, staticvalue)
	local parser = {}
	parser.tokenmt = {
		__index = parser,
		__tostring = token.tostring
	}
	parser.parser = parser					-- the parser (inherited by tokens)
	parser.name = name						-- name of the parser
	parser.staticvalue = staticvalue		-- constant value (only staticvalue for static tokens like keywords and operators)
	return setmetatable(parser, parsermt)
end

--reset this parser so it can attempt to parse the next token
function parser:start()
	self.value = nil
end

--process next character, as well as the entire fragment so far.
-- returns:
-- nil, nil      if this parser is still active
-- nil, error    if this parser has failed. error is an error code (typically a string or true)
-- value, nil    if this parser has completed a match. value is the final matched value (typically a string)
function parser:next(char, fragment)
	if fragment == self.staticvalue then
		return fragment
	end
	local i = #fragment + 1
	if char ~= self.staticvalue:sub(i, i) then
		return nil, true
	end
end

--variant of parser:next that matches a regex pattern instead of a static token
function parser:patternnext(char, fragment)
	local newfrag = fragment..c
	if not newfrag:match(self.anchoredpattern) then
		if #fragment == 0 then
			return nil, true
		else
			return fragment
		end
	end
end

--initialize a new token instance from this parser, with the passed value as that token's value.
function parser:newtoken(value, line, column)
	local token = {
		value = value,
		-- the line and column that this token starts at
		line = line,
		column = column,
	}
	return setmetatable(token, self.tokenmt)
end

-- new parsers will be assigned parsercolor
local parsercolor = "#"
-- use setcolor to change this
local function setcolor(color)
	parsercolor = color or "#"
end

--record of all defined parsers. assign to this table to define new parsers
token.parsers = setmetatable({ setcolor = setcolor }, {
	__newindex = function(self, k, v)
		if type(k) == "string" then
			local parser
			if type(v) == "string" then
				-- parsers["staticvalue"] = "name" defines a static parser
				parser = newparser(v, k)
			elseif type(v) == "function" then
				-- function parsers.name(char, fragment) defines a parser with a custom next() method
				parser = newparser(k)
				parser.next = v
			elseif type(v) == "table" then
				-- parsers.name = {"pattern"} defines a pattern parser
				parser = newparser(k)
				parser.pattern = v[1]
				parser.anchoredpattern = "^"..parser.pattern.."$"
				parser.next = parser.patternnext
			end
			if parser then
				parser.color = parsercolor
				-- store parsers under
				--		static value for static parsers
				--		name for other parsers
				rawset(self, k, parser)
				-- insert new parser at end of list, so order is preserved
				rawset(self, #self + 1, parser)
			end
		else
			rawset(self, k, v)
		end
	end
})

--returns an iterator that scans through a file and returns individual token objects
--or iterator returns error string if token could not be parsed
function token.tokenize(file)
	local line = 1 -- line count
	local column = 0 -- column count
	local next
	local function getnext()
		next = file:read(1)
		if next == "\n" then
			column = 0
			line = line + 1
		else
			column = column + 1
		end
		return next
	end
	getnext()
	return function()
		--skip leading whitespace (no token matches it)
		while next and next:match("%s") do getnext() end
		--if we reached EOF then stop iteration; dont waste time initializing
		if not next then return end
		local startline = line		-- line start count for this token
		local startcolumn = column	-- column start count for this token
		local fragment = ""			-- the total raw text that has been read during this match attempt
		local candidates = {}		-- the set of all parsers still running
		local remaining = #token.parsers		-- the count of remaining candidates
		-- start all the parsers fresh
		for i, parser in ipairs(token.parsers) do
			parser:start()
			candidates[parser] = true
		end
		local lastmatch				-- last matched token
		local lastline					-- line # of the end of the last matched token
		local lastcolumn				-- column # of the end of the last matched token
		local lastcursor				-- position of file cursor after last matched token
		local lastnext					-- the next character after the last matched token
		local lasterror				-- error returned from last failed match
		local lastparser				-- the last parser to fail
		while next do
			for _, parser in ipairs(token.parsers) do
				if candidates[parser] then
					local result, error = parser:next(next, fragment)
					if error then
						candidates[parser] = nil
						remaining = remaining - 1
						lasterror = error
						lastparser = parser
					elseif result then
						candidates[parser] = nil
						remaining = remaining - 1
						lastmatch = parser:newtoken(result, startline, startcolumn)
						lastline = line
						lastcolumn = column
						lastcursor = file:seek()
						lastnext = next
					end
				end
			end
			fragment = fragment..next
			if remaining == 0 then
				if lastmatch then
					lastmatch.failed = false
					line = lastline
					column = lastcolumn
					file:seek("set", lastcursor)
					next = lastnext
					return lastmatch
				else
					getnext()
					return {
						failed = true,
						value = fragment,
						line = startline,
						column = startcolumn,
						error = lasterror,
						parser = lastparser,
					}
				end
			end
			getnext()
		end
	end
end
